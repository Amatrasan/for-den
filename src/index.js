import React from 'react'
import './styles.scss'
import { ReactComponent as Icon } from './icon.svg'

export const Button = ({ text, ...rest }) => {
  return (
    <button className='button' {...rest}>
      <Icon />
      <span>Login</span>
    </button>
  )
}
