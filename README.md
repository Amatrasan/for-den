# for-den

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/for-den.svg)](https://www.npmjs.com/package/for-den) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save for-den
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'for-den'
import 'for-den/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [Amatrasan](https://github.com/Amatrasan)
