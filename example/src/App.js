import React from 'react'

import { Button } from 'for-den'
import 'for-den/dist/index.css'

const App = () => {
  return <Button text="Create React Library Example 😄" />
}

export default App
